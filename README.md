# Introdução ao React Native

## O que é React Native

O React Native é um framework JavaScript criado pelo Facebook que permite acessar a interface e os recursos nativos dos smartphones, ou seja, consiste em uma série de ferramentas que viabilizam a criação de aplicações móveis nativas para as plataformas iOS e Android.

O stack do React Native é poderoso, pois nos permite utilizar **ECMAScript 6**, **Flexbox**, **JSX**, diversos pacotes do **NPM** e muito mais.

## Setup

Em nosso ambiente de desenvolvimento, iremos utilizar o [**Expo**](https://expo.io/).

### O que é o Expo

O Expo é uma ferramenta utilizada no desenvolvimento mobile com React Native que permite o fácil acesso às API’s nativas do dispositivo sem precisar instalar qualquer dependência ou alterar código nativo.

#### Vantagens
- Oferece grande parte de recursos de forma nativa e integrada (câmera, microfone, player de música, etc.);
- Não precisará instalar a SDK do Android ou o XCode para Mac;
- Possui um aplicativo móvel instalável pelas lojas do Android/iOS que contém todo código nativo necessário pelo React Native para iniciar sua aplicação.

#### Desvantagens
- A desvantagem está em pular a etapas de configuração do SDK do Android ou XCode para iOS pois desconhecendo todo esse processo, temos uma limitação futura para lidar com processos de atualização e build das aplicações.

#### Quando utilizar?

- Você está testando o React Native e quer entender como ele funciona;
- Criar apps simples que não serão publicados.

#### Instação e inicialização com Expo

Será necessário o aplicativo Expo para Android/IOS
- [Android](https://play.google.com/store/apps/details?id=host.exp.exponent&referrer=www)
- [IOS](https://itunes.apple.com/app/apple-store/id982107779)

Instala a CLI do Expo de forma global
<br />
```npm install -g expo-cli```

Cria a pasta da aplicação
<br />
```expo init myApp```

Entra na pasta da aplicação
<br />
```cd myApp```

Inicia o servidor de desenvolvimento
<br />
```npm start```

Após este processo, no terminal será gerado um QRCode que será lido pelo aplicativo, iniciando a aplicação direto no smartphone.
<br />
**Importante:** Seu computador e smartphone devem estar conectados na mesma rede.

## Exemplo básico:

```js
import React, { Component } from 'react'
import { View, Text, StyleSheet } from 'react-native'

export default class App extends Component {
	render() {
		return (
			<View style={styles.container}>
				<Text style={styles.f20}>App!</Text>
			</View>
		)
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center',
		backgroundColor: '#fff',
	},
	f20: {
		fontSize: 40
	}
})
```

Perceba que em React Native, nó precisamos importar nossos componentes. Em vez de `div` e `span`, você usará componentes nativos como `View` e `Text`, por exemplo.

Para estilização, será usado `StyleSheet` e a propriedade `style` nos componentes. Falaremos mais de estilo nas próximas aulas.

Note também, que o `core` da aplicação é o mesmo para **React** usado na web e **React Native**!

```js
import React, { Component } from 'react'
```

### Docs:
- [Documentação React Native](https://facebook.github.io/react-native/docs/getting-started)
- [React Native: Construa aplicações móveis nativas com JavaScript](https://tableless.com.br/react-native-construa-aplicacoes-moveis-nativas-com-javascript/)
- [O que você deve saber sobre o funcionamento do React Native](https://tableless.com.br/o-que-voce-deve-saber-sobre-funcionamento-react-native/)
- [Expo](https://expo.io/)
- [Expo: o que é, para que serve e quando utilizar?](https://blog.rocketseat.com.br/expo-react-native/)

### Extra:
Caso você não consiga utilizar o Expo para testar a aplicação, você pode seguir este tutorial com vários modos de configuração:
- [Rocketseat Docs - Ambiente React Native](https://docs.rocketseat.dev/ambiente-react-native/introducao) 

## Desafio

Este desafio terá o mesmo princípio do nosso primeiro desafio em React:

Criar um App que exiba a frase `Hello World!` em 10 formas diferentes e a data atual. Porém dessa vez queremos ver mais JavaScript, pois não teremos CSS. Vocês podem sim usar o `StyleSheet`, mas este não é o foco deste desafio. Queremos JS!

Seja criativo, por exemplo, use letras maiúsculas, minúsculas, de trás pra frente e etc:

#### Inicialização:
- De um Fork no repositório: `https://gitlab.com/acct.fateclab/turma-2-sem-2019/05-introducao-ao-react-native`;
- Agora você deve clonar o repositório localmente, há um botão azul "Clone" no seu repositório do GitLab, clique nele e use a URL com HTTPS: `git clone url-copiada`;
- Agora dentro da pasta clonada, clique com o botão direito do mouse e clique em "Git Bash Here" para entrar na pasta pelo terminal;
- Crie um app com o nome desafio com o Expo: `expo init desafio`;
- Para iniciar o servidor de desenvolvimento não se esqueça de usar `npm run start` e escanear o QRCode no aplicativo Expo para que o App continue sendo buildado sempre que atualizar os arquivos;

#### Entrega: 
- Com as pastas base criadas, reescreva o arquivo App.js para que tenha até 10 textos "Hello World" diferentes. Poderá ser utilizado StyleSheet para alterar o texto, mas foque em JavaScript;
- Faça commits para cada forma diferente de exibição do texto;
- Assim que terminar dê `git push origin seu-nome/introducao-ao-react-native`, crie um Merge Request na interface do GitLab, acesse o menu "Merge Requests" e crie um, configure o "Target Branch" para o repositório original para que seu App seja avaliado e revisado e para que possamos te dar um feedback;
- O nome do Merge Request deve ser o seu nome completo.